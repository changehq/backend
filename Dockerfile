FROM python:3.6

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app
RUN cd
RUN export FLASK_APP=app.py
EXPOSE 5000
ENTRYPOINT ["flask"]
CMD ["run", "-p","5000", "-h","0.0.0.0"]

