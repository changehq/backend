# Backend

This is the backend application.


## How to run this app :  

    1. docker pull registry.gitlab.com/changehq/backend:latest
    2. docker run --rm --it -p 5000:5000 registry.gitlab.com/changehq/backend:latest


## Project Structure :
   
    #### This section will be updated soon.

To request access to the repo, please create an account in https://gitlab.com and share your username in the mailing list. And access will be granted.

## Issues and Milestones :

Backend Milestones are available here : https://gitlab.com/changehq/backend/-/milestones
You can report issues in this link (PS: You would need an account in gitlab.com): https://gitlab.com/changehq/backend/-/issues

Unless there are security issues, all issues will be taken up only on the milestone priority. 





## Gotchas: 
    1. This currently does not have a database integrated, but this will be integrated with postgresql. In the timebeing, once you stop and re-run the project all data would you newly created would be lost.
    2. The swagger is currently enabled openly in production. This will be removed towards the 1.0 release
    3. Gitlab registry has a limitation of 10GB for docker images. This registry will always be cleaned to ensure we dont hit the limits. So always stay on the latest tag. Altough we will be maintaining the last 5 releases in the branch
    4. Currently there is no authentication
## API Tester for UI:

Once you open the application, the swagger UI itself would have the test script, so you wouldnt need any kind of external tool. Please forget postman. Its not worth it :)

