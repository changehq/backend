from flask_restx import Api

from .activity import api as activity_ns
from .pledge import api as pledge_ns


api = Api(
    version='1.0', 
    title='ChangeHQ Backend API',
    description='Backend API for ChangeHQ Project',
)


api.add_namespace(activity_ns)
api.add_namespace(pledge_ns)
