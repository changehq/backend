from flask_restx import Namespace, Resource, fields

api = Namespace('activity', description='Activity operations')


activity = api.model('Activity', {
    'id': fields.Integer(readonly=True, description='The activity unique identifier'),
    'country':fields.List(fields.String,required=True,description="Countries this activity is active in"),
    'description':fields.String(required=True,description="Details of the activity"),
    'user':fields.Integer(required=True,description="User who created the activity"),
    'activity_creation_date':fields.Date(required=True,description="Date this activity was created"),
    'cause_end_date':fields.Date(required=False,description="Date this activity is due to be ended[Currently not being used]"),
    'is_approved':fields.Boolean(required=True,description="Approved"),
    'approved_by':fields.Integer(required=True,description="User who approved the activity"),
    'rejected_reason':fields.String(required=False,description="If this activity was rejected, why was it rejected")
})


class ActivityDAO(object):
    def __init__(self):
        self.counter = 0
        self.Activitys = []

    def get(self, id):
        for Activity in self.Activitys:
            if Activity['id'] == id:
                return Activity
        api.abort(404, "Activity {} doesn't exist".format(id))

    def create(self, data):
        Activity = data
        Activity['id'] = self.counter = self.counter + 1
        self.Activitys.append(Activity)
        return Activity

    def update(self, id, data):
        Activity = self.get(id)
        Activity.update(data)
        return Activity

    def delete(self, id):
        Activity = self.get(id)
        self.Activitys.remove(Activity)

# Sample data loaded. Will be removed once database is integrated
DAO = ActivityDAO()
DAO.create({'country':["IN"],'id':1,'description':"Some activity description will come here",'user':1,'activity_creation_date':"2021-03-13",'cause_end_date':"2199-12-31",'is_approved':True,'approved_by':1,'rejected_reason':None})


@api.route('/')
class ActivityList(Resource):
    '''Shows a list of all Activitys, and lets you POST to add new activitys'''
    @api.doc('list_Activitys')
    @api.marshal_list_with(activity)
    def get(self):
        '''List all activitys'''
        return DAO.Activitys

    @api.doc('create_Activity')
    @api.expect(activity)
    @api.marshal_with(activity, code=201)
    def post(self):
        '''Create a new activity'''
        return DAO.create(api.payload), 201


@api.route('/<int:id>')
@api.response(404, 'Activity not found')
@api.param('id', 'The activity identifier')
class Activity(Resource):
    '''Show a single Activity item and lets you delete them'''
    @api.doc('get_Activity')
    @api.marshal_with(activity)
    def get(self, id):
        '''Fetch a given resource'''
        return DAO.get(id)

    @api.doc('delete_Activity')
    @api.response(204, 'Activity deleted')
    def delete(self, id):
        '''Delete a activity given its identifier'''
        DAO.delete(id)
        return '', 204

    @api.expect(activity)
    @api.marshal_with(activity)
    def put(self, id):
        '''Update a activity given its identifier'''
        return DAO.update(id, api.payload)


if __name__ == '__main__':
    app.run(debug=True)