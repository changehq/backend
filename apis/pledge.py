from flask_restx import Namespace, Resource, fields

api = Namespace('pledge', description='Pledge operations')


Pledge = api.model('Pledge', {
    'id': fields.Integer(readonly=True, description='The Pledge unique identifier'),
    'desc':fields.String(required=True,description="Details of the Pledge"),
    'pledge_start_date':fields.Date(required=True,description="Date this Pledge was created"),
    'pledge_end_date':fields.Date(required=True,description="Date this Pledge was created"),
    'unit':fields.String(required=True,description="Details of the Pledge"),
    'activity':fields.String(required=True,description="Details of the Pledge"),
    'is_track_enabled':fields.Boolean(required=True,description="Details of the Pledge"),
    'amount':fields.Integer(required=True,description="User who approved the Pledge"),
    'currency': fields.String(required=True,description="Currency"),
    'user':fields.Integer(required=True,description="User who created the Pledge"),
})


class PledgeDAO(object):
    def __init__(self):
        self.counter = 0
        self.Pledges = []

    def get(self, id):
        for Pledge in self.Pledges:
            if Pledge['id'] == id:
                return Pledge
        api.abort(404, "Pledge {} doesn't exist".format(id))

    def create(self, data):
        Pledge = data
        Pledge['id'] = self.counter = self.counter + 1
        self.Pledges.append(Pledge)
        return Pledge

    def update(self, id, data):
        Pledge = self.get(id)
        Pledge.update(data)
        return Pledge

    def delete(self, id):
        Pledge = self.get(id)
        self.Pledges.remove(Pledge)

# Sample data loaded. Will be removed once database is integrated
DAO = PledgeDAO()
DAO.create({
    'id': 1,
    'desc':"Contribute 10rs per week",
    'pledge_start_date':"2021-03-13",
    'pledge_end_date':"2021-09-13",
    'unit':"weekly",
    'activity':1,
    'is_track_enabled':True,
    'amount':10,
    'currency':"INR",
    'user':1,
})


@api.route('/')
class PledgeList(Resource):
    '''Shows a list of all Pledges, and lets you POST to add new Pledges'''
    @api.doc('list_Pledges')
    @api.marshal_list_with(Pledge)
    def get(self):
        '''List all Pledges'''
        return DAO.Pledges

    @api.doc('create_Pledge')
    @api.expect(Pledge)
    @api.marshal_with(Pledge, code=201)
    def post(self):
        '''Create a new Pledge'''
        return DAO.create(api.payload), 201


@api.route('/<int:id>')
@api.response(404, 'Pledge not found')
@api.param('id', 'The Pledge identifier')
class Pledge(Resource):
    '''Show a single Pledge item and lets you delete them'''
    @api.doc('get_Pledge')
    @api.marshal_with(Pledge)
    def get(self, id):
        '''Fetch a given resource'''
        return DAO.get(id)

    @api.doc('delete_Pledge')
    @api.response(204, 'Pledge deleted')
    def delete(self, id):
        '''Delete a Pledge given its identifier'''
        DAO.delete(id)
        return '', 204

    @api.expect(Pledge)
    @api.marshal_with(Pledge)
    def put(self, id):
        '''Update a Pledge given its identifier'''
        return DAO.update(id, api.payload)


if __name__ == '__main__':
    app.run(debug=True)