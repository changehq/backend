echo "Enter version number for the build"
read version
docker build -t registry.gitlab.com/changehq/backend:$version .
docker push registry.gitlab.com/changehq/backend:$version
